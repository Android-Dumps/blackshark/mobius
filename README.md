## qssi-user 10 MOBS2005121OS00MP1 V11.0.4.0.JOYUI release-keys
- Manufacturer: blackshark
- Platform: kona
- Codename: mobius
- Brand: blackshark
- Flavor: qssi-user
- Release Version: 10
- Id: MOBS2005121OS00MP1
- Incremental: V11.0.4.0.JOYUI
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: en-GB
- Screen Density: 440
- Fingerprint: blackshark/MBU-H0/mobius:10/MOBS2005121OS00MP1/V11.0.4.0.JOYUI:user/release-keys
- OTA version: 
- Branch: qssi-user-10-MOBS2005121OS00MP1-V11.0.4.0.JOYUI-release-keys
- Repo: blackshark/mobius
