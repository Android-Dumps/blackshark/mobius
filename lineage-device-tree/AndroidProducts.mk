#
# Copyright (C) 2022 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_mobius.mk

COMMON_LUNCH_CHOICES := \
    lineage_mobius-user \
    lineage_mobius-userdebug \
    lineage_mobius-eng
