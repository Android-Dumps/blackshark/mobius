#
# Copyright (C) 2022 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from mobius device
$(call inherit-product, device/blackshark/mobius/device.mk)

PRODUCT_DEVICE := mobius
PRODUCT_NAME := lineage_mobius
PRODUCT_BRAND := blackshark
PRODUCT_MODEL := SHARK MBU-H0
PRODUCT_MANUFACTURER := blackshark

PRODUCT_GMS_CLIENTID_BASE := android-blackshark

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="MBU-H0-user 10 MOBS2005121OS00MP1 V11.0.4.0.JOYUI release-keys"

BUILD_FINGERPRINT := blackshark/MBU-H0/mobius:10/MOBS2005121OS00MP1/V11.0.4.0.JOYUI:user/release-keys
